// To parse this JSON data, do
//
//     final testimonial = testimonialFromJson(jsonString);

import 'dart:convert';
//se asigna un modelo en el cual se extraen los datos desde la pagina de quicktipe
//y se le da este formato 
List<Testimonial> testimonialFromJson(String str) => List<Testimonial>.from(json.decode(str).map((x) => Testimonial.fromJson(x)));

Testimonial testimonialsFromJson(String str) =>
    List<Testimonial>.from(json.decode(str).map((x) => Testimonial.fromJson(x))).first;

String testimonialToJson(List<Testimonial> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Testimonial {
    Testimonial({
        required this.id,
        required this.name,
        required this.location,
        required this.designation,
        required this.avatar,
        required this.message,
        required this.lorem,
        required this.rating,
        required this.audio,
    });

    int id;
    String name;
    String location;
    String designation;
    String avatar;
    String message;
    String lorem;
    double rating;
    String audio;

    factory Testimonial.fromJson(Map<String, dynamic> json) => Testimonial(
        id: json["id"],
        name: json["name"],
        location: json["location"],
        designation: json["designation"],
        avatar: json["avatar"],
        message: json["message"],
        lorem: json["lorem"],
        rating: json["rating"].toDouble(),
        audio: json["audio"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "location": location,
        "designation": designation,
        "avatar": avatar,
        "message": message,
        "lorem": lorem,
        "rating": rating,
        "audio": audio,
    };
}
