import 'package:examentestimonial/models/testimonial.dart';
import 'package:flutter/material.dart';

class TestimonialWidget extends StatelessWidget {
  final Function onTap;
  final Testimonial? testimonial;

  const TestimonialWidget({
    Key? key,
    required this.testimonial,
    required this.onTap,
  }) : super(key: key);
// Se da una funcion de tap al momento de seleccionar un usuario
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap(testimonial!.name, testimonial!.avatar, testimonial!.location);
      },
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Image.network(
              testimonial!.avatar,
              width: 150,
              height: 150,
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(testimonial!.name, style: TextStyle(fontSize: 13.0)),
                Text(testimonial!.location, style: TextStyle(fontSize: 13.0)),
              ],
            ),
          )
        ],
      ),
    );
  }
}
