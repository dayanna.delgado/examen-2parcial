import 'package:examentestimonial/models/testimonial.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

// se hace la conexion y el requerimientos a los datos en este caso toolcartoon por medio del nombre
//tambien se crea dentro de lib las carpetas de data, models , screen, widget para asignar parametros
// y hacer que cada documento realice una funcion de servicios, el detalle , la extraccion de datos y las pantallas
class TestimonialHttpRequest {
  Future<List<Testimonial>> get getTestimonial async {
    final Response response =
        await get(Uri.parse('https://testimonialapi.toolcarton.com/api'));
    return compute(testimonialFromJson, response.body);
  }

  Future<Testimonial> getTestimonials(String name) async {
    final Response response = await get(
        Uri.parse('https://testimonialapi.toolcarton.com/api?name=$name'));
    return compute(testimonialsFromJson, response.body);
  }
}
