import 'package:examentestimonial/models/testimonial.dart';
import 'package:examentestimonial/screen/listatestimonial.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'UOC - Capstone Project',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
            appBar: AppBar(
              title: const Text("Capstone Project"),
            ),
            body: Stack(children: const [
              testimonialListView(),
            ])));
  }
}
