import 'package:examentestimonial/data/dataservicios.dart';
import 'package:examentestimonial/models/testimonial.dart';
import 'package:examentestimonial/widget/gettingWidget.dart';
import 'package:flutter/material.dart';

//se creo la clase de detalle para visualizar los detalles de los datos Json
class TestimonialDetail extends StatefulWidget {
  String name = '';
  String location = '';
  String avatar = '';
  TestimonialDetail(
      {Key? key,
      required this.name,
      required this.location,
      required this.avatar})
      : super(key: key);

  @override
  State<TestimonialDetail> createState() => _TestimonialDetailState();
}
//Se hace una clase para el detalle de los datos
class _TestimonialDetailState extends State<TestimonialDetail> {
  late Future<Testimonial> futureTestimonial;

  @override
  void initState() {
    super.initState();
    futureTestimonial = TestimonialHttpRequest().getTestimonials(widget.name);
  }
// se escriben los datos que se requieren para mostrar en este caso nombre, locacion, designacion y mensaje
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Testimonial>(
      future: futureTestimonial,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const gettingDataWidget();
        }
        Testimonial? miTestimonial = snapshot.data;
        return Scaffold(
          appBar: AppBar(
            title: Text(
              widget.name,
              style: const TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(children: [
                          rowData("Name:", miTestimonial!.name),
                          rowData("Location:", miTestimonial.location),
                          rowData("Designation", miTestimonial.designation),
                          rowData("Message:", miTestimonial.message),
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
//Se edita el estilo de texto
  Row rowData(String label, String data) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Text(
            label,
            style: TextStyle(fontSize: 13.0),
          ),
        ),
        Expanded(
          flex: 5,
          child: Text(
            data,
            style: TextStyle(fontSize: 13.0),
          ),
        ),
      ],
    );
  }
}
