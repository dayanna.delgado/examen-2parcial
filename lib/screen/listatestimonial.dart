import 'package:examentestimonial/data/dataservicios.dart';
import 'package:examentestimonial/models/testimonial.dart';
import 'package:examentestimonial/screen/detalle.dart';
import 'package:examentestimonial/widget/testimoniowidget.dart';
import 'package:flutter/material.dart';

class testimonialListView extends StatefulWidget {
  const testimonialListView({Key? key}) : super(key: key);

  @override
  _testimonialListViewState createState() => _testimonialListViewState();
}

class _testimonialListViewState extends State<testimonialListView> {
  late Future<List<Testimonial>> futureTestimonials;
//se inicia la solicitud de los datos
  @override
  void initState() {
    super.initState();
    futureTestimonials = TestimonialHttpRequest().getTestimonial;
  }
//se socilitan los detalles que se requieren visuzalizar en la pantalla principal
  void showTestimonialDetail(String name, String location, String avatar) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return TestimonialDetail(
        name: name,
        location: location,
        avatar: avatar,
      );
    }));
  }
//se muestra los datos de la lista y se le da formato a la lista y la solicitud de datos a mostrar
  @override
  Widget build(BuildContext context) {
  return FutureBuilder<List<Testimonial>>(
      future: futureTestimonials,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        }
        return ListView.builder(
          itemCount: snapshot.data?.length ?? 0,
          itemBuilder: (context, index) {
            List<Testimonial>? testimonials = snapshot.data;
            return Container(
              margin: const EdgeInsets.fromLTRB(12, 12, 12, 0),
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: 150,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                boxShadow: [
                  BoxShadow(color: Colors.blue, offset: Offset(1, 1))
                ],
              ),
              child: TestimonialWidget(
                testimonial: testimonials![index],
                onTap: showTestimonialDetail,
              ),
            );
          },
        );
      },
    );
  }
}
